package com.crudhibernate.springboot.crud.controller;


import com.crudhibernate.springboot.crud.model.Product;
import com.crudhibernate.springboot.crud.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;


    @GetMapping("/productsGetAll")
<<<<<<< HEAD
    public ResponseEntity<List<Product>> getAllset(){
=======
    public ResponseEntity<List<Product>> getAllitem(){
>>>>>>> 83d5a34 (changing to controller)
        return ResponseEntity.ok().body(productService.getAllProduct());
    }

    @GetMapping("/productsGetById/{id}")
<<<<<<< HEAD
    public ResponseEntity<Product> getProductset(@PathVariable long id){
=======
    public ResponseEntity<Product> getProductitem(@PathVariable long id){
>>>>>>> 83d5a34 (changing to controller)
        return ResponseEntity.ok().body(productService.getProductById(id));
    }

    @PostMapping("/productsAdd")
    public ResponseEntity<Product> create(@RequestBody Product product){
        return ResponseEntity.ok().body(this.productService.createProduct(product));
    }

    
    @PutMapping("/productsPut/{id}")
    public ResponseEntity<Product> update(@PathVariable long id, @RequestBody Product product){
        product.setId(id);
        return ResponseEntity.ok().body(this.productService.updateProduct(product));
    }

}
