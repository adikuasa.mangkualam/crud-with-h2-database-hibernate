package com.crudhibernate.springboot.crud.service;

import com.crudhibernate.springboot.crud.model.Product;

import java.util.List;

public interface ProductService {

    Product createProduct(Product product);
    Product updateProduct(Product product);
    Product updateProduct2(Product product);
    List<Product> getAllProduct();
    Product getProductById(long productId);

}
