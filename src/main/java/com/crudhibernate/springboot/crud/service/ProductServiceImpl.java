package com.crudhibernate.springboot.crud.service;

import com.crudhibernate.springboot.crud.exception.ResourceNotFoundException;
import com.crudhibernate.springboot.crud.model.Product;
import com.crudhibernate.springboot.crud.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product createProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product updateProduct(Product product) {
       Optional<Product> productDb = this.productRepository.findById(product.getId());

        if(productDb.isPresent()) {
            Product productUpdate = productDb.get();
            productUpdate.setId(product.getId());
            productUpdate.setName(product.getName());
            productUpdate.setAddress(product.getAddress());
            productUpdate.getData1();
            productUpdate.getData2();
            productUpdate.setDescription(product.getDescription());
            productRepository.save(productUpdate);
            return productUpdate;
        }else {
            throw new ResourceNotFoundException("Record not Found with id :"+product.getId());
        }

    }

    @Override
    public Product updateProduct2(Product product) {
        Optional<Product> productDb = this.productRepository.findById(product.getId());

        if(productDb.isPresent()) {
            Product productUpdate = productDb.get();
            productUpdate.setId(product.getId());
            productUpdate.setName(product.getName());
            productUpdate.setAddress(product.getAddress());
            productUpdate.setDescription(product.getDescription());
            productRepository.save(productUpdate);
            return productUpdate;
        }else {
            throw new ResourceNotFoundException("Record not Found with id :"+product.getId());
        }

    }

    @Override
    public List<Product> getAllProduct() {
        return this.productRepository.findAll();
    }

    @Override
    public Product getProductById(long productId) {
        Optional<Product> productDb = this.productRepository.findById(productId);

        if(productDb.isPresent()) {
            return productDb.get();
        }else
        {
            throw new ResourceNotFoundException("Record not Found with id :"+productId);
        }
    }
}
