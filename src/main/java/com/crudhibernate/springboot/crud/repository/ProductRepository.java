package com.crudhibernate.springboot.crud.repository;

import com.crudhibernate.springboot.crud.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {


}
